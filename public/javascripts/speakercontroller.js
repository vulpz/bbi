app.controller( 'SpeakerController', function( $scope, $window ) {
	this.speakers = [
		{
			'name': 'Robert Berwick',
			'uni': 'MIT',
			'title': 'There\’s Plenty of Room at the Bottom:',
			'sub': 'Granularity and the Neurobiology of Language - a View from Computer Science',
			'abstract': 'Understanding the “operating system” underlying human language rests on what some have rightly called the “proper granularity” problem and what software and hardware computer designers call the correct “abstraction layers.” Not only is it essential to identify the computational primitives for human language, we must also determine how these might be built into intermediate-size chunks of varying degrees. Here we apply some of the insights from computer science to this problem and how that relates to research into animal communication systems. Evidently, very little of the space of possible intermediate “abstraction layers” has been explored. Concrete examples range from the general ability to “make infinite use of finite means,” to the notion of compilation, that is, the way in which language-related rule systems might be recast for efficient online processing, to the implementation of contemporary “minimalist syntax,” to the application of non-standard memory designs. We outline some challenges these results imply for straightforwardly transferring results from animal communication systems, including artificial language learning experiments, given such potential architectural and computational variation.',
			'website': 'https://lids.mit.edu/people/facultypi/robert-berwick',
			'schedule': 'Sunday 8:30 - 10:30AM  Atrium - Stamp Union'
		},

		{
			'name': 'Bob Dooling',
			'uni': 'University of Maryland',
			'title': 'Species Differences in Perception of Song-Motifs',
			'abstract': 'For decades we have known that both songbirds and humans and songbirds learn complex vocalizations early in life, have a dependence on hearing the adults they will imitate, as well as themselves, in arriving at a species-specific vocal repertoire. For many features of human speech, there are not clear parallels in birdsong such as the capacity for meaning, abstraction, and flexible associations. In other respects there are strikingly strong parallels. I will review some of these parallels and differences on both the perceptual and production side and discuss recent comparative evidence from of sensitivity to the sequential ordering of elements in zebra finches and canaries. Zebra finch song motifs contain syllables that vary in acoustic structure but usually occur in the same fixed sequence. The male bird’s song remains fixed throughout life (so they are closed-ended learners). Zebra finches are highly sensitive to changes in syllable structure but insensitive to changes in syllable order. In comparison, while budgerigars and canaries are also sensitive to syllable structure, only budgerigars are sensitive to syllable order in zebra finch song. One interpretation of these differences may lie in species differences in vocal plasticity. Budgerigars (parrots) are open-ended vocal learners and well-known mimics. Zebra finches and canaries are close- ended learners, though canaries become open-ended during breeding season. These comparative studies raise questions for future research such as: How does song syllable order function in behavioral contexts? And how does the brain encode syllable order?',
			'website': 'https://psyc.umd.edu/facultyprofile/Dooling/Robert',
			'schedule': 'Saturday 8:30 - 10:30AM  Atrium - Stamp Union'
		},

		{
			'name': 'Tecumseh Fitch',
			'uni': 'University of Vienna, Austria',
			'title': 'The Biology and Evolution of Language:',
			'sub': 'Continuity and Change',
			'abstract': 'I investigate human language viewed as a species-typical aspect of our biology, and attempt to understand it via comparison with other species’ cognition and communication systems (the comparative approach). The first step in doing so is to break language down to its components (the multi-component approach) and then ask which components are shared with which other species (or not). I present evidence for continuity in speech perception, most aspects of speech production, and of human conceptual semantics with animal cognition, and evidence for discontinuity when it comes to organizing principles of syntax (hierarchical structure) and potentially some aspects of semantics (pragmatic, theory-of- mind based production).',
			'website': 'http://homepage.univie.ac.at/tecumseh.fitch/',
			'schedule': 'Friday 5 - 7PM  Rm 0224 - St. John Learning Ctr'
		},

		{
			'name': 'Tim Gentner',
			'uni': 'University of California in San Diego',
			'title': 'Generative Models for Acoustic Communication and Natural Language',
			'abstract': 'Despite a number of important findings, current understanding of the neurobiology of language remains focused on patterns of course regional activity correlated with language-relevant tasks. As such we know little of the biological mechanisms or neuro-computational principles that underlie language.  While no animal communication system can serve as a complete model of human language, many of the cognitive processes that support language are nonetheless shared among other species, particularly those that use complex, temporally patterned signals to communicate. Such species offer an opportunity to study language-relevant processes in neurobiological detail. I will discuss recent work from my lab using songbirds, European starlings, to understand categorical perception of complex natural communication signals. Using unsupervised machine learning techniques, we describe the local spectro-temporal structure of, and long range temporal dependencies between, natural acoustic categories in starling song. We find that the temporal dynamics among acoustic categories mirror those observed in other natural languages.  To explore the perception of these categorical signals, we trained starlings to recognize multiple exemplars from different categories. Using the generative capacity of our models, we synthesized smoothly varying sounds that spanned categories, with which we compute psychometric functions in a common acoustic space.  The perceptual attributes of this acoustic space appear to be shared across subjects: different birds place the perceptual boundary between sounds in the same location, and show remarkably similar sensitivity to change across the high-dimensional space that describes the categories. Knowing how the animals’ perception changes along a subset of the models’ acoustic dimensions, allows us to accurately predict perception along other acoustic dimensions in the model.  Finally, we show that the spiking patterns of neurons in the secondary auditory cortical region CM, contribute to the categorical encoding of many different sounds such that collective activity for large populations of CM neurons carries an accurate and categorical representation of the full acoustic-perceptual space.',
			'website': 'http://gentnerlab.ucsd.edu/',
			'schedule': 'Saturday 8:30 - 10:30AM  Atrium - Stamp Union'
		},

		{
			'name': 'Erich Jarvis',
			'uni': 'Rockefeller University',
			'title': 'Evolution of Brain Pathways for Vocal Learning and Spoken Language',
			'abstract': 'Vocal learners such as humans and songbirds can learn to produce elaborate patterns of structurally organized vocalizations, whereas many other vertebrates such as non-human primates and most other bird groups either cannot or do so to a very limited degree. To explain the similarities among humans and vocal-learning birds and the differences with other species, various theories have been proposed. One set of theories are motor theories, which underscore the role of the motor system as an evolutionary substrate for vocal production learning. For instance, the motor theory of speech and song perception proposes enhanced auditory perceptual learning of speech in humans and song in birds, which suggests a considerable level of neurobiological specialization. Another, a motor theory of vocal learning origin, proposes that the brain pathways that control the learning and production of song and speech were derived from adjacent motor brain pathways. Another set of theories are cognitive theories, which address the interface between cognition and the auditory-vocal domains to support language learning in humans. Here we critically review the behavioral and neurobiological evidence for parallels and differences between the so-called vocal learners and vocal non-learners in the context of motor and cognitive theories. In doing so, we note that behaviorally vocal-production learning abilities are more distributed than categorical, as are the auditory-learning abilities of animals. We propose testable hypotheses on the extent of the specializations and cross-species correspondences suggested by motor and cognitive theories. We believe that determining how spoken language evolved is likely to become clearer with concerted efforts in testing comparative data from many non-human animal species.',
			'website': 'http://jarvislab.net/',
			'schedule': 'Friday 5 - 7PM  Rm 0224 - St. John Learning Ctr'
		},

		{
			'name': 'Richard Mooney', 
			'uni': 'Duke University',
			'title': 'From Song to Synapse:',
			'sub': 'Vocal Communication in Songbirds and Mice',
			'abstract': 'The interplay between hearing and vocalization is critical to vocal communication and vocal learning. Recent research using both songbirds and mice has provided useful insights into the neural circuits and mechanisms that mediate this sensorimotor interplay. I will discuss recent progress in understanding how auditory and motor systems interact to enable vocal learning and communication.',
			'website': 'https://www.neuro.duke.edu/mooney-lab/',
			'schedule': 'Saturday 11AM - 12:30PM  Atrium - Stamp Union'
		}, 

		{
			'name': 'Andreas Nieder',
			'uni': 'University of Tubingen, Germany',
			'title': 'The Role of Frontal Lobe Areas in Controlling Vocaliztions in Primates',
			'abstract': 'The question of how human language evolved from our nonhuman primate ancestors has interested neuroscientists for ages. One fundamental and indispensable prerequisite for language is the ability to volitionally control vocalization. We recently showed that nonhuman primates, rhesus macaques, are able to cognitively control their vocalizations in a goal-directed way. Monkeys therefore provide a window of opportunity to explore the neuronal foundations for the initiation of purposeful vocalizations. In monkeys trained to call on command, we recorded single-cell activity from three frontal lobe areas: the ventrolateral prefrontal cortex (vlPFC), the anterior cingulate cortex (ACC) and the pre- supplementary motor area (pre-SMA). All three areas show pre-vocal activity indicative of their participation in volitional call initiation. However, they also show interesting differences with respect to the behavioral relevance and temporal dynamics of neuronal activity that indicate different roles in cognitive and motor functions. The results suggest that executive control structures residing in the vlPFC are coupled to vocal pattern-generating and arousal networks via the medial frontal lobe. These findings agree with our recently proposed “dual neural network model for the evolution of speech and language“ (Hage &amp; Nieder, TINS, 2016) that posits a volitional articulation motor network originating in the prefrontal cortex (including Broca’s area) that cognitively controls vocal output of a phylogenetically conserved primary vocal motor network. On a broader scale, these data suggest that the nonhuman primate is an adequate model for understanding pre-adaptations of higher-level language functions.',
			'website': 'https://homepages.uni-tuebingen.de/andreas.nieder/',
			'schedule': 'Saturday 5 - 6:30PM  Atrium - Stamp Union'
		},

		{
			'name': 'Chris Petkov',
			'uni': 'University of Newcastle, England',
			'title': 'Structured Sequence Learning, Language Evolution and the Primate Brain',
			'abstract': 'Many animals are not thought to be able to combine their vocalizations into structured sequences, as do songbirds, humans and a few other species. Nonetheless, it remains possible that a number of animals are able to recognize certain types of ordering relationships in sequences generated by ‘artificial grammars’. In this talk, I aim to explore how understanding the extent of these hidden sequence learning abilities in animals could help us to unravel the origins of language and be useful for neurobiological pursuits. First, I aim to briefly overview some of our behavioral results with structured sequence processing in three species of primates: marmosets, macaques and humans. I then overview fMRI results identifying frontal regions in macaques that are involved in these forms of sequence processing and compare the results to observations obtained in humans using the same fMRI paradigm. Finally, I show results from a new study involving comparative intracranial recordings in humans and monkeys processing the sequences. The results revealing intriguing neural prediction signals to the sequencing relationships and neural oscillatory dynamics in auditory temporal cortex that are strikingly similar across the species. Overall, the findings indicate that human and non-human primates possess an evolutionarily conserved perisylvian network involved in processing structured auditory input. Alongside the commonalities, there are also indications of cross-species divergences that provide hints on how the human brain differentiated for language.',
			'website': 'http://www.ncl.ac.uk/ion/staff/profile/chrispetkov.html#background',
			'schedule': 'Saturday 5 - 6:30PM  Atrium - Stamp Union'
		},

		{
			'name': 'David Poeppel',
			'uni': 'NYU and Max Planck Institute - Frankfurt',
			'title': 'Be a Splitter, Not a Lumper!',
			'sub': 'On the Proper Granularity to Study the Neurobiology of Language',
			'abstract': 'In the spirit of convenient simplification (even if perversely so), most of us can be classified as splitters or lumpers with respect to our research approaches. If animal communication systems are to play a critical role in understanding human language, I suspect that being a lumper will doom the work from the get-go. Working with categories that are too broad (e.g. “syntax” “semantics”) will render the comparative results — at best — correlational. The good news is that if we embrace, instead, \'radical decomposition\' as a research strategy, i.e. diligently focusing on identifying the elementary particles of the systems (the computational primitives) and their interactions, there is hope that we can understand the operating system at the right level of granularity and generate explanatory understanding. Experimental examples from the neurobiology of language will illustrate this conjecture. There exists, in any case, reason to remain naively optimistic: research on animal communication systems can help us part of the way already, because for certain questions (i.e. at the level of the sensorimotor interfaces) we can use approaches with appropriate spatial resolution and temporal resolution to understand various subroutines. But unless we have the appropriate \'conceptual resolution,\' the relation between animal communication and human language is likely to remain distant and more metaphoric than mechanistic - and some of the deeper questions will remain opaque.',
			'website': 'http://www.psych.nyu.edu/poeppel/',
			'schedule': 'Sunday 8:30 - 10:30AM  Atrium - Stamp Union'
		},

		{
			'name': 'Robert Seyfarth and Dorothy Cheney',
			'uni': 'University of Pennsylvania',
			'title': 'Evolution, Communication, and Cognition Among Primates',
			'abstract': 'For those interested in the evolution of language and human thinking, a logical place to start is the communication and cognition of our closest animal relatives, the nonhuman primates. Nonhuman primates lack language, but they share with humans a long evolutionary history and many homologous brain mechanisms. These are unlikely to have arisen by accident. So we begin with a basic question: how has evolution shaped the communication and cognition of monkeys and apes? Baboons and other primates live in groups where success depends on an individual’s ability to compete, to form cooperative social bonds, and to recognize the bonds that exist among others. To achieve these goals, selection has favored a system of communication in which vocalizations facilitate social interactions by reducing the uncertainty that is inherent whenever individuals interact. They reduce uncertainty by providing information, and they provide information because they have predictive power, or meaning. Listeners derive meaning by combining information from the call, the immediate social context, their memory of past interactions, and their knowledge of the kinship and rank relations among others. The result is a rich, pragmatic system in which communication has both an adaptive, behavioral function and can serve as a system of mental representation. These generalizations are likely to apply broadly, to many species including our pre-linguistic hominoid ancestors.',
			'website': 'https://sites.sas.upenn.edu/seyfarth/',
			'schedule': 'Saturday 2:30 - 4:30PM  Atrium - Stamp Union'
		},

		{
			'name': 'Katie Slocombe',
			'uni': 'York University, England',
			'title': 'Scratching Beneath the Surface:',
			'sub': 'Are Great Ape Signals Produced Intentionally?',
			'abstract': 'Several potentially important similarities have been found between human and animal communication systems, providing possible evidence of continuity, however much less research effort has focussed on whether the cognitive mechanisms underpinning these behaviours are similar. In particular, whether signal production is the result of reflexive processes, or can be characterised as intentional is highly debated. Criteria which might be used to identify intentional signal production will be discussed, before outlining recent studies that have applied these criteria to chimpanzee vocal behaviour.',
			'website': 'https://www.york.ac.uk/psychology/staff/academicstaff/ks553/',
			'schedule': 'Saturday 2:30 - 4:30PM  Atrium - Stamp Union'
		},

		{
			'name': 'Peter Tyack',
			'uni': 'University of St. Andrews, Scotland',
			'title': 'A Comparative Perspective on Vocal Learning in Mammals',
			'abstract': 'Humans learn to speak by listening to acoustic models, forming auditory templates, and then learning to produce vocalizations that match the template. A similar pattern has been found for many songbirds, but most other animal species are thought to inherit vocal motor programs and to develop vocalizations with an acoustic structure that is not influenced by auditory feedback. The evolutionary origins of vocal learning among our hominid ancestors are obscure, because no non- human primates show strong evidence for vocal learning. Along with three taxa of birds, five mammalian groups have evolved vocal learning: bats, cetaceans (or dolphins and whales), elephants, humans and seals. Some of the best evidence involves animals reared by humans that learn to imitate speech sounds. These abilities are well known for parrots and some songbirds, but also have been demonstrated for a harbor seal and an Asian elephant. The songs of some wild birds and mammals also provide evidence of learning – for example all adult males within a population of humpback whales sing similar songs at one time, and track changes in the song over months and years. As with some songbirds, there is evidence for phonological syntax in the whales. Humpback song can more accurately be classified when sounds are segmented into subunits that are shorter than most individual utterances. Among killer whales each group has a group-distinctive set of calls, but each call may be made up of specific sequences of subunits, with some evidence that subunits are shared across populations that have no acoustic contact. This raises questions about whether learning these vocalizations requires formation of a new auditory template or whether the animal simply needs to remember a specific sequence of subunits that may be part of a species-specific repertoire. We do know that bottlenose dolphins can learn a new auditory template and produce vocalizations that match, because they can imitate the arbitrary frequency modulation pattern of a computer-generated tonal signal. This skill is used in the wild when dolphins learn to imitate the individually distinctive signature whistles of social partners. Several strands of evidence suggest that signature matching is used by one individual dolphin to address a specific partner within a group, and that the partner responds preferentially when its signature is imitated. Imitated signature whistles thus are learned signals that are used to label an individual where the association between individual and label is also learned. This kind of labeling with learned signals is important for human language but has seldom been described for animal communication systems. A focus on vocal learning among mammals thus helps bring a comparative focus on elements of language as diverse as phonological syntax and reference.',
			'website': 'https://risweb.st-andrews.ac.uk/portal/en/persons/peter-lloyd-tyack(6efef907-a94d-4a78-9ba9-4999ab4fb5d7).html',
			'schedule': 'Saturday 11AM - 12:30PM  Atrium - Stamp Union'
		}, 

		{
			'name': 'Sandy Waxman',
			'uni': 'Northwestern University',
			'title': 'Becoming Human:',
			'sub': 'How (and how early) Do Infants Link Language and Cognition?',
			'abstract': 'Language is a signature of our species. To acquire a language, infants must identify which signals are part of their language and discover how these are linked to the objects and events they encounter and to their core representations. For infants as young as 3 months of age, listening to human vocalizations promotes the formation of object categories, a fundamental cognitive capacity. Moreover, this precocious link emerges from a broader template that initially encompasses vocalizations of human and non-human primates, but is rapidly tuned specifically to human vocalizations. In this talk, I’ll focus on the powerful contributions of both ‘nature’ and ‘nurture’ as infants discover increasingly precise links between language and cognition, and use them to learn about their world. I’ll also tie in ideas about communication in non-human animals.',
			'website': 'http://www.psychology.northwestern.edu/people/faculty/core/profiles/sandra-waxman.html',
			'schedule': 'Sunday 8:30 - 10:30AM  Atrium - Stamp Union'
		},

		{
			'name': 'Stephanie White',
			'uni': 'University of California, Los Angeles',
			'title': 'Network-based Approaches to Vocal Learning',
			'abstract': 'Socially-learned vocal communication signals are often best acquired during developmental critical periods. A great deal of progress has been made toward understanding the molecular basis for critical periods underlying cortical processing of primary sensory information. Whether these processes extend to sensorimotor integration and how sub-cortical regions such as the basal ganglia may contribute is unknown. Thanks to decades of neuroethological study, birdsong is the leading model for human speech development, having established numerous key parallels to human vocal learning. These include shared critical periods and reliance on cortico-basal ganglia circuitry. A great experimental attribute of the songbird model is that within cortex, basal ganglia and thalamus, sub- regions dedicated to vocal learning are visibly clustered, forming tractable targets for linking brain and behavior. We have leveraged this organization to identify spatio-temporal profiles of the song- dedicated transcriptome in the basal ganglia. Our findings suggest a new model whereby the intersection of learning related gene co-expression modules intersect with song related modules to provide a permissive nexus for vocal learning.',
			'website': 'https://whitelab.ibp.ucla.edu/people/',
			'schedule': 'Saturday 8:30 - 10:30AM  Atrium - Stamp Union'
		},

		{
			'name': 'Klaus Zuberbuhler',
			'uni': 'St. Andrews, Scotland',
			'title': 'Intentional Communication in Primates',
			'abstract': 'Human language is the product of underlying intentions that are purposely expressed, epistemically monitored and flexibly interpreted. What are the origins of this capacity? Evolutionary transitions are gradual events, suggesting that intention may also have evolved in different grades. I will review some current empirical literature to suggest that goal-directed intentionality is well within the scope of animals, while mind-directed intentionality appears to be restricted to great apes and humans. Finally, humans may be unique in their capacity to share their intentional states with each other.',
			'website': 'https://risweb.st-andrews.ac.uk/portal/en/persons/klaus- zuberbuehler(5fe7c3e4-ac93- 4218-9291- aace93790c85).html',
			'schedule': 'Saturday 2:30 - 4:30PM  Atrium - Stamp Union'
		}
	]

	this.goToPage = function( $event ) {
		$window.open( $event.speaker.website, '_blank' )
	}
} )