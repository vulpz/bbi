app.controller( 'ProgramController', function( $scope, $window ) {
	this.sections = [ 
		{
			'name': 'Schedule',
			'link': '/schedule'
		},
		{
			'name': 'Speakers',
			'link': '/Speakers'
		}
	]

	this.goToPage = function( $event ) {
		$window.location.href = $event.section.link
	}
} )