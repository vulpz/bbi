const app = angular.module( 'BBIApp', ['smoothScroll'] )
	.factory( 'scrolld', function( smoothScroll ) {
		var options = {
				duration: 700,
				easing: 'easeOutQuad'
			}
		var service = {}
		service.scrollTo = function( to ) {
			smoothScroll( to, options )
		}
		return service
	} )