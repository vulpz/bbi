app.controller( 'NavController', function( $scope, $window, scrolld ) {
	this.show_nav = true
	this.menu_items = []

	var home = {
		'name': 'Home',
		'link': '/'
	}

	var schedule = {
		'name': 'Schedule',
		'link': '/schedule'
	}

	var speakers = {
		'name': 'Speakers',
		'link': '/speakers'
	}

	var register = {
		'name': 'Register',
		'link': 'register'
	}

	var travel = {
		'name': 'Travel',
		'link': 'travel'
	}

	var sponsors = {
		'name': 'Sponsors',
		'link': 'sponsors'
	}

	var contact = {
		'name': 'Contact',
		'link': 'contact'
	}

	this.menu_items.push( home, schedule, speakers, travel, register, sponsors, contact )

	this.clickIt = function( $event ) {

		if( $event.item.link.startsWith( '/' ) ) {
			$window.location.href = $event.item.link
		} else if( $window.location.href != 'http://bbiworkshop.umd.edu/' ) {
			$window.location.href = '/#' + $event.item.link
		} else {
			scrolld.scrollTo( document.getElementById( $event.item.link ) )
		}
	}
} )