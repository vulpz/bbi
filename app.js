var express = require( 'express' )
var path = require( 'path' )
var favicon = require( 'serve-favicon' )
var logger = require( 'morgan' )
var cookieParser = require( 'cookie-parser' )
var bodyParser = require( 'body-parser' )

var app = express()

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use( logger( 'dev' ) )
app.use( bodyParser.json() )
app.use( bodyParser.urlencoded( { extended: false } ) )
app.use( cookieParser() )
app.use( express.static( path.join( __dirname, 'public' ) ) )

app.set( 'view engine', 'ejs' )
app.set( 'views', path.join( __dirname, 'views' ) )

app.get( '/', ( req, res, next ) => {
	res.render( 'index.ejs', {
	    heroimg: '../images/mandrill_low.jpg',
	    navcontroller: 'NavController',
	    programcontroller: 'ProgramController'
  	} )
} )

app.get( '/schedule', ( req, res, next ) => {
	res.render( 'schedule.ejs', {
		heroimg: '../images/spiza_low.jpg',
		navcontroller: 'NavController'
	} )
} )

app.get( '/speakers', ( req, res, next ) => {
	res.render( 'speakers.ejs', {
		heroimg: '../images/chimp_low.jpg',
		navcontroller: 'NavController'
	} )
} )

app.get( '/dining', ( req, res, next ) => {
	res.render( 'dining.ejs', {
		heroimg: '../images/mandrill_low.jpg',
		navcontroller: 'NavController'
	} )
} )

app.listen( 80, 'bbiworkshop.umd.edu', () => {
	console.log( 'Listening..' )
} )
